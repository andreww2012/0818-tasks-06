﻿class Circle {
private:
	double radius;
	double ference;
	double area;

public:
	Circle(double);
	void set_radius(double);
	void set_ference(double);
	void set_area(double);
	
	double get_ference(void);
	double get_area(void);
};