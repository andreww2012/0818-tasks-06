﻿/*
1 Разработать класс Circle (круг), содержащий следующие поля:

Radius - радиус Ference - длина окружности Area - площадь круга

Данные поля рекомендуется объявить как переменные типа double и с модификатором доступа private для безопасности. Доступ к этим полям следует ограничить с помощью методов класса.

В класс Circle можно включить конструктор, принимающий значение радиуса.

при установке значения радиуса пересчитывать длину окружности и пощадь;
при установке длины окружности пересчитывать радиус и площадь;
при установке площади пересчитывать радиус и длину окружности.

*/
#define _USE_MATH_DEFINES
#include <math.h>
#include "task06-1.h"

Circle::Circle(double r) {
	radius = (r >= 0) ? r : 0;
	ference = 2 * M_PI * radius;
	area = M_PI * radius * radius;
}

void Circle::set_radius(double r) {
	radius = (r >= 0) ? r : 0;
	ference = 2 * M_PI * radius;
	area = M_PI * radius * radius;
}

void Circle::set_ference(double f) {
	ference = (f >= 0) ? f : 0;
	radius = ference / (2 * M_PI);
	area = M_PI * radius * radius;
}

void Circle::set_area(double a) {
	area = (a >= 0) ? a : 0;
	radius = sqrt(area / M_PI);
	ference = 2 * M_PI * radius;
}

double Circle::get_ference() {
	return ference;
}

double Circle::get_area() {
	return area;
}